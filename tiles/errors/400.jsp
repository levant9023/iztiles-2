<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 7/16/15
  Time: 5:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="request" value="${pageContext.request}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="baseURL" value="${request.scheme}://${request.serverName}:${request.serverPort}${request.contextPath}"
       scope="request"/>

<c:set var="messages" value="${requestScope.messages}"/>

<html lang="<c:out value="${lang}"/>">
<head>
  <base href="${baseURL}/">
  <meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description"
        content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results. Search easy with izSearch!">
  <meta name="keywords"
        content="search engine, web search, privacy, private search, image search, video search, search engine privacy, search engine optimization, search engine marketing, easy search">
  <title>iZSearch: 404 error</title>
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
  <link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/errors.css"/>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <c:if test="${not empty messages}">
    <div id="error-widget" class="panel panel-default">
      <div class="panel-heading">
        <h1>
          <span class="fa-stack fa-lg">
            <i class="fa fa-square fa-stack-2x"></i>
            <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
          </span>
          <span class="error-title">${messages["title"]}</span> <span class="error-status">${messages["status"]}</span>
        </h1>
      </div>
      <div class="panel-body">
        <p class="lead message">${messages["message"]}</p>
        <p class="descr">May be your request string is malformed or too long.</p>
        <div id="search-panel" class="form-group">
          <form id="search-form" class="navbar-form navbar-left autosuggest" role="search"
                action="${baseURL}" method="GET" target="_self">
            <input id="input-search" type="text" class="form-control" autocomplete="off" name="q" value="<c:out value="${query}"/>" autofocus>
            <button id="btn-search" type="submit" class="btn btn-primary">Search</button>
            <div id="autosuggest" class="form-group">
              <ul id="autocomplete" class="autocomplete" style="display: none;"></ul>
            </div>
          </form>
        </div>
      </div>
      <div class="panel-footer">
        <a class="btn btn-md btn-default" onclick="window.history.back();return false;" role="button">Back</a>
        <a class="btn btn-md btn-default" href="${baseURL}/" role="button">To Home</a>
      </div>
    </div>
  </c:if>
</div>
</body>
</html>
