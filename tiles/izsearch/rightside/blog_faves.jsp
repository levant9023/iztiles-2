<%-- 
    Document   : faves
    Created on : Sep 2, 2014, 6:34:32 AM
    Author     : efanchik
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<c:set var="faves_map" value="${requestScope.faves}" scope="page"/>
<c:if test="${not empty faves_map}" >
  <c:set var="faves" value="${faves_map['faves']}" />
  <c:set var="tab_index" value="0" />

  <div class="blogblock wpanel">
    <c:if test="${not empty faves}" >
      <c:forEach var="tab" items="${faves}">
        <c:choose>
          <c:when test="${(fn:length(tab.value) gt 0) && (tab.key == 'blog') }">
            <c:set var="capTab" value="${fn:toUpperCase(fn:substring(tab.key, 0, 1))}" />
            <c:set var="bodyTab" value="${fn:substring(tab.key, 1, fn:length(tab.key))}" />
            <div class="faves" id="faves-img-${tab.key}">
              <div class="newFaves">
                <div id="faves-tabs-${tab.key}" style="border-bottom:0; padding:0">
                  <ul class="nav nav-tabs" role="tablist">
                    <c:forEach var="fave" items="${tab.value}" varStatus="faves_status">
                      <c:set var="cap" value="${fn:toUpperCase(fn:substring(fave.key, 0, 1))}" />
                      <c:set var="body" value="${fn:substring(fave.key, 1, fn:length(fave.key))}" />
                      <c:set var="liclass" value="" />
                      <c:if test="${faves_status.index == 0 }" >
                        <c:set var="liclass" value="active"/>
                      </c:if>
                      <li class="${liclass}"><a href="#tabs-faves-${tab.key}-${tab_index+faves_status.index}" role="tab" data-toggle="tab"><c:out value="${cap}${body}" default="N/A"/></a></li>
                    </c:forEach>
                  </ul>
                  <div class="tab-content">
                    <c:forEach var="fave" items="${tab.value}" varStatus="faves_status">
                      <c:set var="divclass" value="" />
                      <c:if test="${faves_status.index == 0 }" >
                        <c:set var="divclass" value="active"/>
                      </c:if>
                      <div id="tabs-faves-${tab.key}-${tab_index+faves_status.index}" class="tab-pane fade in ${divclass}">
                        <ul class="faves-list">
                          <c:set var="faveslist2draw" value="${fave.value}" scope="request"/>
                          <jsp:include page="faves.jsp"/>
                        </ul>
                      </div>
                    </c:forEach>
                  </div>
                </div>
              </div>
            </div>
            <c:set var="tab_index" value="${tab_index+fn:length(tab.value)}"/>
          </c:when>
        </c:choose>
      </c:forEach>
    </c:if>
  </div>

</c:if>