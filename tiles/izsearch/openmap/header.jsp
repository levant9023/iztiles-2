<%-- 
  Document: header
  User:     eFanchik
  Date:     07.07.14
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="count" value="${(empty requestScope.c) ? 10 : requestScope.c}" scope="request"/>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="type" value="${empty requestScope.t ? '' : requestScope.t}" scope="request"/>
<c:set var="offset" value="${empty requestScope.offset ? '0' : requestScope.offset}" scope="request"/>
<c:set var="records" value="${empty requestScope.records ? '10' : requestScope.records}" scope="request"/>
<c:set var="date_start" value="${empty requestScope.start ? '' : requestScope.start}" scope="request"/>
<c:set var="date_stop" value="${empty requestScope.stop ? '' : requestScope.stop}" scope="request"/>
<c:set var="sort" value="${requestScope.sort}" scope="request"/>
<c:set var="sortTitle" value="Sort by date"/>

<c:set var="requestURL" value="${baseURL}/${requestScope.wpath}" scope="request"/>

<c:choose>
  <c:when test="${empty sort}">
    <c:set var="sort" value="date:desc"/>
    <c:set var="sortTitle" value="Sort by date" />
  </c:when>
  <c:otherwise>
    <c:if test="${sort == 'date:desc'}">
      <c:set var="sort" value=""/>
      <c:set var="sortTitle" value="Sort by relevance" />
    </c:if>
  </c:otherwise>
</c:choose>

<c:url var="sortUrl" value="${baseURL}">
  <c:param name="q" value="${query}"/>
  <c:param name="c" value="${count}"/>
  <c:param name="offset" value="${offset}"/>
  <c:param name="records" value="${records}"/>
  <c:param name="start" value="${date_start}"/>
  <c:param name="stop" value="${date_stop}"/>
  <c:param name="sort" value="${sort}"/>
</c:url>

<script type="text/javascript">
    var offset = "${offset}";
    var records = "${records}";    
</script>

<div id="header" class="row">
  <nav class="navbar navbar-default no-back" role="navigation">        
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div id="logo" class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" 
                data-toggle="collapse" data-target="#nvb-01">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="/" class="navbar-brand">
          <img class="logo" src="/resources/img/slf_logo_40.png" alt="image">
        </a>
      </div>
      <form id="search-form" class="navbar-form navbar-left" role="search" 
            action="${requestURL}" method="GET" target="_self">
        <input id="input-search" type="text" class="form-control" name="q" 
               value="<c:out value="${query}"/>" autofocus="true">
        <input type="hidden" name="c" value="<c:out value="${count}"/>">
        <input type="hidden" name="offset" value="<c:out value="${offset}"/>" >
        <input type="hidden" name="records" value="<c:out value="${records}"/>" >
        <button id="btn-search" type="submit" class="btn btn-primary">Search</button>        
      </form>
    </div><!-- /.container-fluid -->
  </nav>
</div>
