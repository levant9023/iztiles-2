<%--
  User: efanchik
  Date: 6/8/15
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!-- #sideRight -->
<aside id="right-wrapper" class="right_container col-xs-3 col-sm-3">
  <div id="rsads-wrapper" class="row" style="padding-top: 3px;"></div>
  <div class="row">
    <div class="content sticky">
      <div id="jimbotron"></div>
      <div id="comments"></div>
    </div>
    <!-- content -->
  </div>
</aside>
<!-- #sideRight -->
