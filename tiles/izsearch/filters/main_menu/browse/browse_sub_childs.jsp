<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 30.01.15
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${level < 3}">
  <c:set var="level" value="${level+1}"/>
  <c:forEach var="children" items="${node.children}">
    <c:set var="title" value="${fn:toLowerCase(children.key)}"/>
    <span class="spt">${title}</span>
  </c:forEach>
</c:if>

