<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 29.01.15
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/videos.css" />

<c:set var="news" value="${requestScope.lastImages}" />
<section class="art-section"></section>