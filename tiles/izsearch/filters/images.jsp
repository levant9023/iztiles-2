<%--
    Document   : videos_filter
    Created on : Nov 11, 2014, 3:26:47 AM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/images.css"/>

<c:if test="${not empty requestScope.images}">
  <c:set var="bing" value="${requestScope.images['bing']}" />
  <c:set var="flickr" value="${requestScope.images['flickr']}" />
  <c:set var="getty" value="${requestScope.images['getty']}" />
  <c:set var="google" value="${requestScope.images['google']}" />
 <ul class="imagelist">

   <c:if test="${not empty bing}">
     <c:forEach var="elems" items="${bing}" varStatus="status">
       <c:forEach var="image" items="${elems.images}">
         <c:url var="img_url" value="${image}"/>
         <li>
         <a href="${img_url}" class="fancybox" rel="gallery">
           <img src="" data-original="${img_url}" class="lazy imagestyle" alt="image"/>
         </a>
         </li>
       </c:forEach>
     </c:forEach>
   </c:if>

   <c:if test="${not empty getty}">
     <c:forEach var="elems" items="${getty}" varStatus="status">
       <c:forEach var="image" items="${elems.images}">
         <c:url var="img_url" value="${image}"/>
         <li>
           <a href="${img_url}" class="fancybox" rel="gallery">
             <img src="" data-original="${img_url}" class="lazy imagestyle" alt="image"/>
           </a>
         </li>
       </c:forEach>
     </c:forEach>
   </c:if>

   <c:if test="${not empty flickr}">
     <c:forEach var="elems" items="${flickr}" varStatus="status">
       <c:forEach var="image" items="${elems.images}">
         <c:url var="img_url" value="${image}"/>
         <li>
           <a href="${img_url}" class="fancybox" rel="gallery">
             <img src="" data-original="${img_url}" class="lazy imagestyle" alt="image"/>
           </a>
         </li>
       </c:forEach>
     </c:forEach>
   </c:if>

   <c:if test="${not empty google}">
     <c:forEach var="elems" items="${google}" varStatus="status">
       <c:forEach var="image" items="${elems.images}">
         <c:url var="img_url" value="${image}"/>
         <li>
           <a href="${img_url}" class="fancybox" rel="gallery">
             <img src="" data-original="${img_url}" class="lazy imagestyle" alt="image"/>
           </a>
         </li>
       </c:forEach>
     </c:forEach>
   </c:if>
  </ul>

  <script>
    $(document).ready(function () {
      $("img.lazy").lazyload({
        event: "scrollstop"
      });
    })
  </script>
</c:if>