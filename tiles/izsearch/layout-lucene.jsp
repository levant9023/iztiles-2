<%@ page import="org.omg.CORBA.Request" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="titles" uri="http://tiles.apache.org/tags-tiles" %>


<!DOCTYPE HTML>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>

<html lang="<c:out value="${lang}"/>">
<head>
  <base href="${baseURL}/">
  <meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description"
        content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results.">
  <meta name="keywords"
        content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">
  <title>iZSearch:&nbsp;<c:out value="${meta_title}" default=""/></title>
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
  <link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap-datetimepicker.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/js/production/bootstrap/css/jquery-ui.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/css/izsearch/custom.css"/>
  <%--<link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/css/izsearch/media.css"/>--%>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/js/production/css/owl.carousel.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/js/production/css/owl.theme.css"/>

  <link rel="stylesheet" type="text/css" media="print" href="${baseURL}/resources/css/izsearch/print.css"/>
  <link rel="stylesheet" type="text/css" media="print" href="${baseURL}/resources/js/production/css/owl.carousel.css"/>
  <link rel="stylesheet" type="text/css" media="print" href="${baseURL}/resources/js/production/css/owl.theme.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="${baseURL}/resources/js/production/plugins/popover/popover.css"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquerypp.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/moment-locales.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/modules/owl.carousel.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/plugins/popover/popover.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/plugins/slick-1.5.9/slick.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/controls/left_navigation.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/controls/map_images.js"></script>

  <!-- Google Analytics counter -->
  <script>
      (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
              (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-58665066-1', 'auto');
      ga('require', 'linkid', 'linkid.js');
      ga('send', 'pageview');
      setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
  </script>

  <script type="text/javascript">
      $(document).ready(function(){
          new LNav('#lnav-wrapper', {});
      });
      var query = "<c:out value='${query}' default=''/>";
      var lang = "<c:out value='${lang}' default='en'/>";
      var geo = {};
  </script>
  <script type="text/javascript" data-main="${baseURL}/resources/js/production/main.js" src="${baseURL}/resources/js/production/require.js"></script>
</head>

<body>

<!-- wrapper -->
<div id="wrapper" class="container-fluid">
  <tiles:insertAttribute name="header"/>
  <section id="middle" class="row">
    <tiles:insertAttribute name="leftblock"/>
    <tiles:insertAttribute name="centerblock"/>
    <tiles:insertAttribute name="rightblock"/>
    <tiles:insertAttribute name="rightads"/>
  </section>
</div>

<tiles:insertAttribute name="footer"/>

<h1 class="grob">${requestScope.meta_title}</h1>
<h2 class="grob">${requestScope.meta_title}</h2>
<div class="popover-hide" style="display: none;"></div>

<%--<script type="text/javascript" defer src="${baseURL}/resources/js/production/mvc/controls/news.js"></script>--%>
<script type="text/javascript" defer src="${baseURL}/resources/js/production/mvc/controls/rss.js"></script>
<script type="text/javascript" defer src="${baseURL}/resources/js/production/mvc/controls/advertise/adv.js"></script>
<script type="text/javascript" defer src="${baseURL}/resources/js/production/mvc/controls/amazon_ads.js"></script>
</body>
</html>
