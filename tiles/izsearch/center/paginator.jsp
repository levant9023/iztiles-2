<%--  
  User: eFanchik
  Date: 07.07.2014
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@page import="org.izsearch.helper.Paginator" %>

<%
    // context vars
    int offset = (Integer) request.getAttribute("offset");
    int records = (Integer) request.getAttribute("records");
    int count = (Integer) request.getAttribute("c");
    String basepath = request.getAttribute("baseURL").toString();
    String wpath = request.getAttribute("wpath").toString();

    if (count > 0) {

        int PERPAGE = 20;
        int maxrecords = 500;

        if (records != PERPAGE) {
            records = PERPAGE;
        }
        String params = "&records=" + records;
        params += "&c=" + count;
        String href = basepath + wpath;
        Paginator pager = null;
        int numPages = (maxrecords - 1) / PERPAGE + 1;
        if (numPages > 1) {
            try {
                pager = new Paginator(href, params, maxrecords, PERPAGE, offset, 10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (pager != null) {
            out.print(pager.getPaginator());
        }
    }
%>
