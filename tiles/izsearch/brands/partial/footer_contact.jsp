<footer class="site-footer ftr">
  <div class="bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-xs-12 text-lg-left text-center">
          <p class="copyright-text">&#169; 2018 <a href="http://izsearch.com">izsearch.com</a></p>
        </div>

        <div class="col-lg-6 col-xs-12 text-lg-right text-center">
          <ul class="list-inline">
            <li class="list-inline-item">
              <a href="${baseURL}/brand/intro.html">Home</a>
            </li>
            <li class="list-inline-item">
              <a href="${baseURL}/brand/features.html">Features</a>
            </li>
            <li class="list-inline-item">
              <a href="${baseURL}/brand/why.html">Why iZ Brands?</a>
            </li>
            <li class="list-inline-item">
              <a href="${baseURL}/brand/method.html">Method</a>
            </li>
            <li class="list-inline-item">
              <a href="${baseURL}/brand/contact.html">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>