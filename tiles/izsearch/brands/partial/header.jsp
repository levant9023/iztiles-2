<!-- Header -->
<header id="header">
  <div class="container">

    <div id="logo" class="pull-left">
      <a href="https://izsearch.com"><img src="${baseURL}/resources/img/brands/izbrands_logo_short.png" alt="logo"/></a>
      <a class="brands" href="${baseURL}/brand/intro.html">BRANDS</a>
    </div>

    <nav id="nav-menu-container">
      <ul class="nav-menu">
        <li><a href="${baseURL}/brand/features.html">Features</a></li>
        <li><a href="${baseURL}/brand/why.html">Why iZ Brands?</a></li>
        <li><a href="${baseURL}/brand/method.html">Method</a></li>
        <li><a href="${baseURL}/brand/contact.html">Contact</a></li>
      </ul>
    </nav>
    <!-- #nav-menu-container -->

    <nav class="nav social-nav pull-right d-none d-lg-inline">
      <a href="https://www.twitter.com/izsearch"><i class="fa fa-twitter"></i></a> <a href="https://www.facebook.com/izsearchcom"><i class="fa fa-facebook"></i></a>
      <a href="https://www.linkedin.com/company/izsearch"><i class="fa fa-linkedin"></i></a> <a href="${baseURL}/brand/contact.html"><i class="fa fa-envelope"></i></a>
    </nav>
  </div>
</header>
<!-- #header -->