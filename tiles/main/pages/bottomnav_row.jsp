<%-- 
    Document   : bottomnav_row
    Created on : Oct 2, 2014, 12:38:23 AM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:url var="features_url" value="features.html" />
<c:url var="ads_url" value="ads.html" />
<c:url var="private_url" value="private.html" />
<c:url var="community_url" value="community.html" />


<div id="bottomnav-row" class="row">
  <div class="panel panel-default panel-motto no-border">
      <div class="panel-body horizontal-middle no-padding">
          <ul class="pager"> 
            <li>
              <a href="${features_url}" class="flat no-border">
                <span class="glyphicon glyphicon-flag glyph-lg"></span><br />
                <span class="btn-suscr">More features</span>
              </a>
            </li>
            <li>
              <a href="${ads_url}" class="flat no-border" title="">
                <span class="glyphicon glyphicon-th-list glyph-lg"></span><br />
                <span class="btn-suscr">Less ads</span>
              </a>
            </li>
            <li>
              <a href="${private_url}" class="flat no-border" title="">
                  <span class="glyphicon glyphicon-user glyph-lg"></span><br />
                  <span class="btn-suscr">Private search</span>
              </a>
            </li>
            <li>
              <a href="${community_url}" class="flat no-border" title="">
                <span class="glyphicon glyphicon-globe glyph-lg"></span><br />
                <span class="btn-suscr">Open community</span>
              </a>
            </li>
          </ul>
      </div>
  </div>
</div>
