<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="data" value="${requestScope.data}" scope="page"/>

<div class="header">
  <div id="logo-block">
    <a class="logo" href="${baseURL}/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"><br>&copy;iZSearch</a>
    <h1>Picking the Keywords</h1>
  </div>
  <div id="user-profile">
    <ul id="user-profile-list">
      <li><div class="user-name">Hello <c:out value="${data['username']}" default="Guest" /></div></li>
      <li><div class="user-email"></div></li>
      <li><div class="user-image"></div></li>
      </ul>
      <a id="btn-password-change" class="btn btn-primary">Change password</a>
      <a id="logout" href="${baseURL}/j_spring_security_logout" class="btn btn-primary"><span class="glyphicon glyphicon-log-out"></span> Log out</a>
  </div>
</div>