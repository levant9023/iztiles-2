<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%-- <tiles:insertAttribute name="topnav-row"/> --%>
<div class="slider_controls">
  <span class="pin_position pin" data-toggle="tooltip" data-placement="left" title="Fix this view as Your default page" data-original-title="Fix this view as Your default page"><i class="fa fa-thumb-tack" aria-hidden="true"></i></span>
  <span id="sliders_trigger" title="Roll over"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
</div>
<div id="ifb" class="info dropdown">
  <button id="ifb_button" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <img class="show_info" src="${baseURL}/resources/img/start_page/menu-button.svg">
  </button>
  <ul class="nav">
    <li><a href="${baseURL}/about.html"><i class="glyphicon glyphicon-info-sign"></i>About</a></li>
    <li><a href="http://blog.izsearch.com"><i class="glyphicon glyphicon-pencil"></i>Blog</a></li>
    <li><a id="bbtn-api" href="${baseURL}/api.html"><i class="btn-suscr glyphicon glyphicon-bullhorn"></i>API</a></li>
    <li><a id="brand-api" href="${baseURL}/brand/intro.html"><i class="btn-suscr glyphicon glyphicon-random"></i>iZ Brands</a></li>
    <li><a id="bbtn-advertise" href="${baseURL}/advertise.html"><i class="glyphicon glyphicon-tags"></i>iZ Ads</a></li>
    <%--<li><a id="bbtn-izebra" href="${baseURL}/app.html"><i class="fa fa-android"></i> iZebra App</a></li>--%>
    <li><a id="bbtn-investor" href="${baseURL}/investor_relations.html"><i class="fa fa-users" aria-hidden="true"></i>Investors</a></li>
    <li><a id="bbtn-engine" data-toggle="modal" data-target="#btn_engine"><i class="btn-suscr glyphicon glyphicon-plus"></i>Add to Search</a></li>
    <li><a id="bbtn-homepage" data-toggle="modal" data-target="#btn_homepage"><i class="btn-suscr glyphicon glyphicon-home"></i>Set as Home</a></li>
    <li>
      <div class="social-ft">
      <a class="share-square ss-fb" target="_blank" href="https://www.facebook.com/izsearchcom/"></a>
      <a class="share-square ss-tw" target="_blank" href="https://twitter.com/izsearch"></a>
      <a class="share-square ss-ytb" target="_blank" href="https://www.youtube.com/channel/UCNDCQTaWEUx1P9466WLnv1A" ></a>
      </div>
    </li>
  <li><p class="cc">© 2018 iZSearch</p></li>
    <!--<img class="arrow" style="right: 0" src="/resources/img/nav/arrow.png">-->
  </ul>
</div>

<tiles:insertAttribute name="search-row"/>
<%-- <tiles:insertAttribute name="motto-row"/> --%>
<%-- <tiles:insertAttribute name="bottomnav-row"/> --%>
