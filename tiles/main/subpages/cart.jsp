<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>iZSearch search engine | Cart</title>
    <link rel="shortcut icon" type="image/png" href="/resources/img/favicon.png">
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/cart.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
</head>
<body>
    <div class="header">
        <div id=header-cont>
            <a  class="logo" href="/"><img  src="http://izsearch.com/resources/img/logo.svg"></a>
            <ul>
                <li><a href="#">My advertisements</a></li>
                <li><a href="#">New advertisements</a></li>
            </ul>
            <div id="cart">
                <li><a href="#"><i class="glyphicon glyphicon-shopping-cart"></i>Cart</a> <span>  |  </span></li>

                <li><a href="#">My account<i class="glyphicon glyphicon-user"></i></a></li>
            </div>
        </div>
    </div>
    <div id="container">
        <div id="shop_cart">
            <span><i class="glyphicon glyphicon-shopping-cart">Shopping Cart()</i></span>
        </div>
        <table class="table_product">
            <tbody>
            <tr>
                <th>Product description</th>
                <th>Term</th>
                <th>Plan</th>
                <th>Cost</th>
            </tr>
            <tr>
                <td>New Advertisements</td>
                <td>
                    <select type="text" id="year" class="param param-mkt form-control" >
                        <option>1 Year</option>
                        <option>2 Year</option>
                        <option>3 Year</option>
                    </select>

                </td>
                <td>
                    <select type="text" id="year1" class="param param-mkt form-control" >
                        <option>Mustard</option>
                        <option>Ketchup</option>
                        <option>Relish</option>
                    </select>

                </td>
                <td>Cost</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total</td>
            </tr>
            </tbody>
        </table>
        <button id="checkout" type="button" class="btn btn-default">Proceed to Checkout</button>
        <div id="coupon">
            <span class="coupon-text">Enter coupon code</span>
            <div class="col-lg-6">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Coupon code...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button">OK</button>
          </span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>