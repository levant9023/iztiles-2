<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">Is iZSearch compatible with Windows 10 / Edge?</h1>
      <h3>Is iZSearch compatible with Windows 10 / Edge?</h3>
      <p>iZSearch is now compatible with Windows 10/Edge.</p>
      <p>To add iZSearch as a default search engine in Edge, please open your Edge browser, and follow the
        instructions given here:  
        <a href="https://iZSearch.com/eng/download-iZSearch-plugin.html?&hmb=1">https://iZSearch.com/eng/download-iZSearch-plugin.html?&hmb=1</a>
        To add iZSearch as Edge's homepage, you can follow these instructions:</p>
      <ol>
        <li>Click on the three dots (the <b>More actions</b> button) in the top right.</li>
        <li>Click on the <b>Settings</b> menu item.</li>
        <li>Locate the <b>Open</b> with section.</li>
        <li>Select the A <b>specific page or pages option</b>.</li>
        <li>Click on the dropdown menu below and choose Custom.</li>
        <li>Click the <b>X</b> cross beside any pages you do not want (ie. about:start).</li>
        <li>Enter: <a href="https://iZSearch.com">https://iZSearch.com</a> into the text-box, and click the
          plus sign + to add. (Please note, you can also decide to enter a settings URL in this field. To learn
          more about <a href="https://iZSearch.com/eng/download-iZSearch-plugin.html?&hmb=1">this</a> option,
          please visit this article from our Support Center).
        </li>
        <li>Restart Microsoft Edge to see iZSearch.com open.</li>
      </ol>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
