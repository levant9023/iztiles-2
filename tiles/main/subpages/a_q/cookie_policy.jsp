<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">What is iZSearch's cookie policy?</h1>
      <h3>What is iZSearch's cookie policy?</h3>
      <p>iZSearch does not use tracking cookies, and we do not access or review cookies placed on your browser by other websites.</p>
      <p>If you use the iZSearch Settings page and save your preferences, or if you change the language setting from our home page, a single anonymous cookie is used to save your search preferences. If you do not wish to receive that cookie, you can use the "URL Generator" feature on our Settings page instead.</p>
      <p>
      Two optional iZSearch features also use temporary session-only cookies. These cookies are not stored by iZSearch and they disappear at the close of the session:
      <ul>
        <li>When you're on the classic.iZSearch.com site, iZSearch gives you the option of refining your search to exclude results you have already seen (The "Search within results" feature at the bottom of a search results page). If used, this sets a session-only cookie, which is not logged and which disappears at the end of the session. That cookie contains one-way-encrypted indicators of the previously displayed results to support this feature. The "lossy encryption" technique makes it impossible to determine which result was clicked on after the fact. That information is also not stored.</li>
        <li>Certain websites offer partner-specific versions of iZSearch to their users. In this case, if a user searches iZSearch from that website, that website's logo might appear in the iZSearch header. In those cases, a session-only cookie containing the partner code is set, allowing iZSearch to display that logo on subsequent searches in the same session. At the close of the session the cookie disappears.</li>
      </ul>
      </p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
