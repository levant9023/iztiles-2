<%--
    Document   : bug_report
    Author     : Vlad L-I-S
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/subpages/bugreport.css" />
<jsp:useBean id="fields" class="java.util.ArrayList" scope="request" />
<c:set value="${requestScope.result}" var="issues"/>
<c:if test="${not empty requestScope.result}">
    <c:if test="${not empty issues}">

        <ul id="issues-list" class="list-group">

            <c:forEach var="issue" items="${issues}" varStatus="status">

                <c:set var="desc" value='${issue["description"]}'/>

                <c:url var="issue_url" value="/bugreport.html">
                  <c:param value="${issue['id']}" name="pid" />
                </c:url>

                <c:if test="${not empty issue['custom_fields']}" var="cf" >
                    <c:set value="${issue['custom_fields']}" var="fields"/>
                </c:if>

                <c:set var="subject" value='${issue["subject"]}'/>

                <c:set value="" var="date" />
                <c:if test="${not empty issue['updated_on']}" var="updated_date">
                    <c:set var="date" value="${fn:substring(issue['updated_on'], 0, 10)}" />
                </c:if>

                <c:set var="status_css" value="unknown" />
                <c:set var="status" value="unknown" />

                <c:if test="${not empty issue['status']['id']}" var="state">
                    <c:set var="status" value="${issue['status']['name']}" />
                    <c:choose>
                        <c:when test="${issue['status']['id'] == 1}">
                            <c:set var="status_css" value="new" />
                        </c:when>
                        <c:when test="${issue['status']['id'] == 2}">
                            <c:set var="status_css" value="in_progress" />
                        </c:when>
                        <c:when test="${issue['status']['id'] == 3}">
                            <c:set var="status_css" value="resolved" />
                        </c:when>
                        <c:when test="${issue['status']['id'] == 4}">
                            <c:set var="status_css" value="feedback" />
                        </c:when>
                        <c:when test="${issue['status']['id'] == 5}">
                            <c:set var="status_css" value="closed" />
                        </c:when>
                        <c:when test="${issue['status']['id'] == 6}">
                            <c:set var="status_css" value="rejected" />
                        </c:when>
                    </c:choose>
                </c:if>
                <li class="list-group-item">
                    <div class="issue-group">
                        <div class="issue-item-header-group">
                            <div class="issue-title">
                                <a class="link" href="/comments.html?pid=${issue['id']}" target="_blank"><c:out value="${subject}" default="N/A" escapeXml="false" /></a>
                            </div>
                            <div class="issue-date">
                                <span class="status-name">&nbsp;<span class="status-body ${status_css}"><c:out value="${status}" default="unknown"/></span></span>
                            </div>
                        </div>
                        <div class="issue-item-center-group">
                            <div class="left"><c:out value="${iz:linkify(iz:url_decode(desc))}" default="" escapeXml="false"/></div>
                        </div>
                        <div class="infoUser">
                            <div class="right">
                                Created by <span class="lnkc"><c:out value="${fields[1]['value']}" default="N/A"/></span>, <c:out value="${date}" default="" />
                            </div>
                        </div>
                    </div>
                </li>
            </c:forEach>
        </ul>
    </c:if>
</c:if>
<div id="rsads-wrapper" class="row" style="padding-top: 3px;"></div>