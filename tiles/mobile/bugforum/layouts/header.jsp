<%--
  User: Vlad L-I-S
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>


<c:set var="btn_sh" value="${cookie['btn_sh'].value}" scope="page"/>
<c:set var="requestURL" value="${baseURL}"/>

<c:choose>
    <c:when test="${btn_sh eq 1}">
        <c:set var='visible' value='style="display: block"' />
    </c:when>
    <c:otherwise>
        <c:set var='visible' value='style="display: hidden"' />
    </c:otherwise>
</c:choose>

<script type="text/javascript" async data-main="${baseURL}/resources/js/production/mobile/js/mvc/filters_app.js" src="${baseURL}/resources/js/production/require.js"></script>

<div id="header">
    <div id="search-block">
        <ul id="search-nav-items" class="nav-items">
            <li id="logo-item">
                <a id="logo-link" href="${baseURL}" title="">
                    <img class="logo-img input-group-sm" src="${baseURL}/resources/img/bugreport.png" alt="search">
                </a>
            </li>
            <li id="search-item">
                <form id="search-form" action="${requestURL}" method="GET" target="_self">
                    <div class="input-group input-group-xs">
                        <input id="input-search" type="text" class="form-control" autocomplete="off" name="q" value="<c:out value="${query}"/>" autofocus>
						<span class="search-btn"><i class="fa fa-search"></i></span>
                    </div>
                </form>
                <div id="autosuggest">
                    <ul id="autocomplete" class="autocomplete"></ul>
                </div>
            </li>
        </ul>
    </div>

    <div class="icons-menu">
        <div>
            <button id="btn-menu" type="button" class="btn btn-default btn-lg btn-list">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
        <div class="ricons">
            <button id="remine-link" type="button" class="btn btn-default btn-lg btn-list">
                <span class="glyphicon glyphicon-envelope"></span>
            </button>
        </div>
        <div class="ricons">
            <a id="redmine-blog" href="bugreport.html?page=1" class="btn btn-default btn-lg btn-list" title="Bug report and Discussion Forum">
                <span class="glyphicon glyphicon-list-alt"></span>
            </a>
        </div>
    </div>
</div>
