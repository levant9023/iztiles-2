<%--
  User: efanchik
  Date: 8/2/15
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<c:set var="request" value="${pageContext.request}" scope="request"/>

<div class="container-fluid-full">
  <tiles:insertAttribute name="left_nav" />
  <tiles:insertAttribute name="central" />
</div>
